import { Dish } from './../../models/dish.model';
import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dish',
  templateUrl: './dish.component.html',
  styleUrls: ['./dish.component.scss']
})

export class DishComponent {

  @Input() item: Dish;
  @Output() selectEvent: EventEmitter<number> = new EventEmitter<number>();
  @Output() dblEvent = new EventEmitter<Dish>();

  constructor() {}

  public onDishSelect(): void {
    this.selectEvent.emit(this.item.id);
  }

  public onDblClick(event: MouseEvent): void {
    event.preventDefault();
    event.stopPropagation();
    this.dblEvent.emit(this.item);
  }
}
