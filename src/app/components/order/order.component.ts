import { Dish } from './../../models/dish.model';
import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit, OnDestroy {

  @Input() order: Dish[];
  @Output() deleteEvent = new EventEmitter<Dish>();

  constructor() {}

  ngOnInit() {
    console.log('Order component создан');
  }

  ngOnDestroy() {
    console.log('Order component удален');
  }

  public get summ(): number {
    let total = 0;
    this.order.forEach(item => {
      total += item.price;
    });
    return total;
  }

  public deleteItem(item: Dish): void {
    this.deleteEvent.emit(item);
  }

}
